package gnut3ll4.com.netspressomobile;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements Callback {

    private Button buttonToggleCafetiere;
    private Button buttonGetStatus;
    private TextView textViewDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        buttonToggleCafetiere = (Button) findViewById(R.id.btn_toggle_cafetiere);
        buttonGetStatus = (Button) findViewById(R.id.btn_get_status);
        textViewDisplay = (TextView) findViewById(R.id.tv_etat);


        buttonToggleCafetiere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OkHttpClient client = new OkHttpClient();

                DateTime now = new DateTime();
                MediaType mediaType = MediaType.parse("application/octet-stream");
                String data = "{ " +
                        "\"box\": {       " +
                        "\"name\" : \"netspresso01\"," +
                        "\"state\" : \"Ready\"},                       " +
                        "\"event\": {    " +
                        "\"uuid\":\"d63c8131-dc60-5145-88ef-7f1cbad1e12e\",    " +
                        "\"event_id\":38,    " +
                        "\"resource_event_id\":37,    " +
                        "\"calendar_id\":6,    " +
                        "\"user_id\":7,    " +
                        "\"username\":\"Laurin, Patrick\",    " +
                        "\"start_time\":\"" + getDateToAPIFormat(now, 0) + "\",    " +
                        "\"end_time\":\"" + getDateToAPIFormat(now, 5) + "\",    " +
                        "\"subjet\":\"Test\",    " +
                        "\"status\":\"CONFIRMED\",    " +
                        "\"ready_time\":\"" + getDateToAPIFormat(now, 0) + "\",    " +
                        "\"stdby_time\":\"" + getDateToAPIFormat(now, 5) + "\"}}";
                RequestBody body = RequestBody.create(mediaType, data);



                Request request = new Request.Builder()
                        .url("http://cedille.etsmtl.ca:8080/go/event.json")
                        .post(body)
                        .addHeader("cache-control", "no-cache")
                                .build();


                OkHttpHandler okHttpHandler = new OkHttpHandler(request);
                String result = "";
                try {
                    result = okHttpHandler.execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                textViewDisplay.setText(result);
            }
        });


        buttonGetStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Request request = new Request.Builder()
                        .url("http://cedille.etsmtl.ca:8080/go/status.json?box=netspresso01")
                        .get()
                        .addHeader("cache-control", "no-cache")
                        .build();

                String result = "";

                OkHttpHandler handler = new OkHttpHandler(request);
                try {
                    result = handler.execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                textViewDisplay.setText(result);


            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFailure(Request request, IOException e) {

    }

    @Override
    public void onResponse(Response response) throws IOException {


        textViewDisplay.setText(response.body().string());


    }

    public String getDateToAPIFormat(DateTime datetime, int minutesToAdd) {
        datetime = datetime.plusMinutes(minutesToAdd);

        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.print(datetime);
    }


}
