package gnut3ll4.com.netspressomobile;

import android.os.AsyncTask;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/**
 * Created by gnut3ll4 on 23/11/15.
 */
public class OkHttpHandler extends AsyncTask<Void, Void, String> {

    OkHttpClient client = new OkHttpClient();
    Request request;

    public OkHttpHandler(Request request) {
        this.request = request;
    }

    @Override
    protected String doInBackground(Void... params) {

        try {

            Response response = client.newCall(request).execute();
            return response.body().string();

        } catch (Exception e) {
        }

        return null;
    }
}